"use strict";
module.exports = (app) => {
    let webHook = require("../controllers/webhookController");

    app.route("/webhook")
        .post(webHook.handleNotification);
};
