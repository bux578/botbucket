"use strict";

let request = require("request");

let discordWebhookUrl = process.env.DISCORDWEBHOOKURL;

exports.handleNotification = function (req, res) {

    let data = req.body;
    if (!data) {
        res.sendStatus(404);
    }

    if (!!data.resource) {
        let message = data.detailedMessage.markdown;

        if (!!discordWebhookUrl) {
            request({
                method: "post",
                url: discordWebhookUrl,
                json: {
                    "content": message
                },
                headers: {
                    "content-type": "application/json"
                }
            }, function (error, response, body) {
                //console.log(response);
            });
        } else {
            console.error("Discord Webhook Url not set in process.env.DISCORDWEBHOOKURL");
            res.sendStatus(500);
        }

    }
    res.sendStatus(200);
};
